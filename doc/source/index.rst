.. Flag Raster Renderer documentation master file

Bit Flag Renderer
=================

The Bit Flag Renderer is a QGIS Plugin to visualize bit flags in raster quality images.

The Bit Flag Renderer is developed at Humboldt-Universität zu Berlin,
`Earth Observation Lab <https://hu-berlin.de/eo-lab>`_ as part of the
`Land Use Monitoring System (LUMOS) <https://eo.belspo.be/en/stereo-in-action/projects/remote-sensing-image-processing-algorithms-land-use-and-land-cover>`_
project, funded by the Belgian Science Policy Office as part of the Stereo-III research program (grant no. SR/01/349).


Features
--------

* Renders single flags and combinations of neighbored flags in raster images
* integrated to QGIS Layer Styling panel


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   How to use <usage.rst>
   About <about.rst>
   Changes <changelog.rst>
   License <LICENSE.md>


.. table::

   ==================== ================================================================================================
   Online documentation https://readthedocs.org/projects/bit-flag-renderer/
   Source Code          https://bitbucket.org/jakimowb/bit-flag-renderer
   Issue tracker        https://bitbucket.org/jakimowb/bit-flag-renderer/issues
   ==================== ================================================================================================

License and Use
---------------

This program is free software; you can redistribute it and/or modify it under the terms of the `GNU General Public License Version 3 (GNU GPL-3) <https://www.gnu.org/licenses/gpl-3.0.en.html>`_ , as published by the Free Software Foundation. See also :ref:`License`.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
