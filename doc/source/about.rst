
About
-----

The Bit Flag Renderer is a QGIS Plugin to visualize bit flags in raster quality images.

The Bit Flag Renderer is developed at Humboldt-Universität zu Berlin,
`Earth Observation Lab <https://hu-berlin.de/eo-lab>`_ as part of the
`Land Use Monitoring System (LUMOS) <https://eo.belspo.be/en/stereo-in-action/projects/remote-sensing-image-processing-algorithms-land-use-and-land-cover>`_
project, funded by the Belgian Science Policy Office as part of the Stereo-III research program (grant no. SR/01/349).
